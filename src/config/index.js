import {pageD3Graph, pageHome} from '../Pages';

export const routes = [
    {
        to: '/',
        headerName: 'Главная',
        cmp: pageHome
    },
    {
        to: '/graph',
        headerName: 'Граф',
        cmp: pageD3Graph
    }
];
