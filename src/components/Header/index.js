import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {routes} from '../../config';
import './style.css'

export default class Header extends Component{
    createMenu = () => routes.map(el => <li key={el.to}>
                    <Link to={el.to}>
                        {el.headerName}
                    </Link>
                </li>);

    render() {
        return (
            <ul className='header'>
                {this.createMenu()}
            </ul>
        )
    }
}
