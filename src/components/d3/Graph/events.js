import * as d3 from 'd3';
import {lineSettings} from './settings';

export function appendEvents (nodes, events) {
    events.forEach(el => {
        nodes.on(el.name, el.event)
    });
    return nodes
}
export function onHover () {
    updateMousedNode.call(this, true)
}

export function onUnHover () {
    updateMousedNode.call(this, false)
}
function updateMousedNode(value) {
    const element = this;

    const sourcedTargeted = d3.selectAll(`line[source][target]`);
    const connections = sourcedTargeted.filter(el => el.source.id === element.__data__.id || el.target.id === element.__data__.id);
    const nodes = d3.selectAll('circle[data_id]:not(.beautify-node)');
    const connectedNodes = nodes.filter(node => connections.data().find(el => el.target.id === node.id || el.source.id === node.id));
    const hoveredWrapper = d3.select(element);
    const hoveredNode = hoveredWrapper.select('.beautify-node');
    const hoveredText = hoveredWrapper.select('foreignObject');
    const duration = 250;
    const progressNodes = connectedNodes.filter(function () {
        return this.getAttribute('class') === 'progress-bar'
    });

    hoveredNode.attr('fill', function () {
        if (value) {
            return hoveredNode.attr('fill_preview')
        } else {
            return '#FFF'
        }
    });

    hoveredText.attr('opacity', value ? 0.5 : 1);

    connections.transition()
        .duration(duration)
        .attr('stroke-width', function () {
            return value ? this.getAttribute('saved_stroke_width') : 1
        })
        .attr('opacity', function () {
            return value ? this.getAttribute('saved_opacity') : lineSettings.lineMinOpacity
        });

    connectedNodes.transition()
        .duration(duration)
        .attr('fill', value ? '#4c98e9' : '#ffffff');

    progressNodes.transition()
        .duration(500)
        .attr('opacity', function f () {
            return value ? this.getAttribute('saved_opacity') : lineSettings.lineMinOpacity
        })
        .attr('style', function (d) {
            const r = +this.getAttribute('r');
            const c = Math.PI * (r * 2);
            const val = +this.getAttribute('weight_percent');
            const pct = ((100 - val) / 100) * c;

            return `stroke-dashoffset: ${value ? pct : lineSettings.progressDashArrayValue}px;`
        })

}
