import {nodeSettings, lineSettings, getOpacityByWeight, getStrokeWidthByWeight} from './settings';
import {progressSettings, getProgressOpacityByWeight, getPercentByWeight} from './settings';

export function createWrapper(svg) {
    return svg.append('g')
}
export function createNode(wrapper, nodes) {
    return wrapper.append('g').selectAll('.node')
        .data(nodes)
        .enter().append('g')
        .attr('class', 'node')
        .attr('data_id', d => d.id)

}
export function createCircle (nodes, beautify) {
    return nodes.append('circle')
        .attr('stroke-width', 1)
        .attr('node_type', beautify ? 'beauty' : 'main')
        .attr('class', (d) => {
            if (beautify) {
                return 'cursor-pointer beautify-node'
            }
            return 'cursor-pointer'
        })
        .attr('stroke', d => this.centerNodes.includes(d) ? '#4c98e9' : '#000')
        .attr('r', beautify ? nodeSettings.nodeMaxR - 3 : nodeSettings.nodeMaxR)
        .attr('data_id', d => d.id)
        .attr('id', d => `node_${d.id}`)
        .attr('fill', d => beautify && this.centerNode.id === d.id ? '#4c98e9' : '#ffffff')
        .attr('fill_preview', d => `url(#preview_${d.id})`)
}
export function createTitle (nodes) {
    return nodes.append('foreignObject')
        .attr('width', nodeSettings.nodeMaxR * 2)
        .attr('height', nodeSettings.nodeMaxR * 2)
        .attr('x', -nodeSettings.nodeMaxR)
        .attr('y', -nodeSettings.nodeMaxR)
        .attr('text-anchor', 'middle')
        .append('xhtml:p')
        .text(d => d.name)
        .attr('class', 'text-in-circle')
}

export function createDef (svg, links) {
    return svg.append('svg:defs').selectAll('marker')
        .data(links)
        .enter().append('svg:marker')
        .attr('id', 'marker_arrow')
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 0)
        .attr('refY', 0)
        .attr('stroke', '#fff')
        .attr('stroke-width', 1)
        .attr('markerWidth', d => 2)
        .attr('markerHeight', d => 2)
        .attr('orient', 'auto')
        .append('svg:path')
        .attr('d', 'M0,-5L10,0L0,5')
}
export function createLink (wrapper, links) {
    return wrapper.append('g')
        .attr('stroke', '#000000')
        .selectAll('line')
        .data(links)
        .enter().append('line')
        .attr('pathLength', 1)
        .attr('opacity', lineSettings.lineMinOpacity)
        .attr('saved_opacity', d => getOpacityByWeight(d.weight, this.graphData[this.step].maxLinkWeight))
        .attr('saved_stroke_width', d => getStrokeWidthByWeight(d.weight, this.graphData[this.step].maxLinkWeight))
        .attr('target', d => d.target.id)
        .attr('source', d => d.source.id)
        .attr('stroke-width', 1)
        .attr('marker-end', d => {
            return d.isDirected ? 'url(#marker_arrow)' : ''
        })
}
export function createProgressBar (nodes) {
    return nodes.append('circle')
        .attr('stroke-width', 3)
        .attr('class', 'progress-bar')
        .attr('stroke', '#4c98e9')
        .attr('r', nodeSettings.nodeMaxR + 2)
        .attr('weight_percent', d => getPercentByWeight(d.weight, this.graphData[this.step].maxWeight))
        .attr('saved_opacity', d => getProgressOpacityByWeight(d.weight, this.graphData[this.step].maxWeight))
        .attr('data_id', d => d.id)
        .attr('id', d => `node_${d.id}`)
        .attr('fill', 'transparent')
        .attr('transition', 'stroke-dashoffset 3s linear')
        .attr('stroke-dasharray', progressSettings.progressDashArrayValue)
        .attr('stroke-dashoffset', 0)
        .attr('style', `stroke-dashoffset: ${progressSettings.progressDashArrayValue}px;`)
}
