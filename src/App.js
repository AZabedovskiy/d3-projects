import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {routes} from './config';
import Header from './components/Header'
import './App.css';

class App extends Component {
    createRoutes = () => routes.map(el => <Route exact path={el.to} component={el.cmp} key={el.to} />);

    render() {
        return (
            <Router>
                <div className='App'>
                    <Header/>
                    <div className='main'>
                        {this.createRoutes()}
                    </div>
                </div>
            </Router>
    );
    }
}

export default App;
