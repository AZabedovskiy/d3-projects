import React, {Component} from 'react'
import * as d3 from 'd3'
import './style.css'
import {initial} from './staticData';
import {
    createNode,
    createWrapper,
    createCircle,
    createTitle,
    createDef,
    createLink,
    createProgressBar,
} from './elements';
import {getStrokeWidthByWeight} from './settings';
import {appendEvents, onHover, onUnHover} from './events';

export default class Graph extends Component{
    constructor() {
        super();
        this.svg = React.createRef();
        this.simulation = null;
        this.wrapper = React.createRef();
        this.nodes = initial.nodes;
        this.links = initial.links;
        this.centerNodes = [];
        this.centerNode = initial.nodes[0];
        this.step = 0;
        this.graphData = {};
        this.setGraphData(initial)
    }
    render() {
        return (
            <div className='graph-wrapper' ref={this.wrapper}>
                <svg id='graph' ref={this.svg}></svg>
            </div>
        )
    }
    initGraph() {
        const scope = this;
        this.simulation =this.forceSimulation(this.nodes, this.links).on('tick', ticked);
        this.svg = d3.select(this.svg.current).attr('viewBox',
            [
                -this.wrapper.current.offsetWidth / 2,
                -this.wrapper.current.offsetHeight / 2,
                this.wrapper.current.offsetWidth,
                this.wrapper.current.offsetHeight
            ]
        );
        const wrapper = createWrapper(this.svg);
        let nodeEvents = [
            {name: 'mouseover', event: onHover},
            {name: 'mouseout', event: onUnHover}
        ];
        const node = appendEvents(createNode(wrapper, this.nodes), nodeEvents);
        createCircle.call(this, node);
        createCircle.call(this, node, true);
        createTitle(node);
        createDef(this.svg, this.links);
        createProgressBar.call(this, node)
        const link = createLink.call(this, wrapper, this.links);
        function ticked() {
            node.attr('transform', d => `translate(${d.x}, ${d.y})`);
            const radDebug = 6;

            function calcSize(d, destination, ord) {
                const altDestination = destination === 'source' ? 'target' : 'source';
                let distance = Math.sqrt(Math.pow(d.source.x - d.target.x, 2) + Math.pow(d.source.y - d.target.y, 2));
                distance = !distance ? 0.1 : distance;
                const distance2 = +d3.select(`circle[data_id='${d[destination].id}']`).node().getAttribute('r') + (getStrokeWidthByWeight(d.weight, scope.graphData[scope.step].maxLinkWeight) + radDebug);
                const ratio = distance2 / distance;
                const dx = ((d[altDestination][ord] - d[destination][ord]) * ratio);
                return d[destination][ord] + dx
            }

            link
                .attr('x1', function (d) {
                    return calcSize(d, 'source', 'x')
                })
                .attr('y1', d => {
                    return calcSize(d, 'source', 'y')
                })
                .attr('x2', d => {
                    return calcSize(d, 'target', 'x')
                })
                .attr('y2', d => {
                    return calcSize(d, 'target', 'y')
                })

        }

    }
    forceSimulation(nodes, links) {
        return d3.forceSimulation(nodes)
            .force('link', d3.forceLink(links).id(d => d.id).distance(100))
            .force('center', d3.forceCenter())
            .force('transition', d3.transition)
            .force('charge', d3.forceManyBody().strength(d => -1500))
            // .alpha(12)
    }
    setGraphData(payload) {
        ++this.step;
        let weight = 0;
        payload.nodes.forEach(el => {
            if (el.weight > weight) weight = el.weight
        });
        let linkWeight = 0;
        payload.links.forEach(el => {
            if (el.weight > linkWeight) linkWeight = el.weight
        });

        this.graphData[this.step] = payload;
        this.graphData[this.step].centerNode = payload.nodes[0];
        this.centerNodes.push(payload.nodes[0]);
        this.graphData[this.step].maxWeight = weight;
        this.graphData[this.step].maxLinkWeight = linkWeight;
    }
    componentDidMount() {
        this.initGraph()
    }
    componentWillUnmount() {
        this.simulation.stop()
        this.simulation = null
        this.nodes = []
        this.links = []
    }
}
