export const nodeSettings = {
    nodeMaxR: 30
};
export const lineSettings = {
    lineMinOpacity: 0.1,
    lineMaxWidth: 3,
    lineMinWidth: 0.15,

};
export const progressSettings = {
    progressDashArrayValue: 210,
    progressMinOpacity: 0.4
};
export function getStrokeWidthByWeight (val, maxWeight) {
    let relPercent = 100 / (maxWeight / val);
    relPercent = relPercent > 100 ? 100 : relPercent;
    const result = (lineSettings.lineMaxWidth / 100) * relPercent;
    return result < lineSettings.lineMinWidth ? lineSettings.lineMinWidth : result
}
export function getOpacityByWeight (val, maxOpacity) {
    const relPercent = 100 / (maxOpacity / val);
    const result = (1 / 100) * relPercent;
    return result < lineSettings.lineMinOpacity ? lineSettings.lineMinOpacity : result
}
export function getPercentByWeight (val, maxWeight) {
    return 100 / (maxWeight / val)
}
export function getProgressOpacityByWeight (val, maxOpacity) {
    const relPercent = 100 / (maxOpacity / val);
    const result = (1 / 100) * relPercent;
    return result < progressSettings.progressMinOpacity ? progressSettings.progressMinOpacity : result
}
