export const initial = {
    nodes: [
        {
            name: 'Example',
            weight: 5,
            id: 1,
        },
        {
            name: 'Example1',
            weight: 10,
            id: 2,
        },
        {
            name: 'Example2',
            weight: 15,
            id: 3,
        },
        {
            name: 'Example3',
            weight: 20,
            id: 4,
        },
        {
            name: 'Example4',
            weight: 25,
            id: 5,
        }
    ],
    links: [
        {
            source: 1,
            target: 2,
            weight: 0.23123
        },
        {
            source: 1,
            target: 3,
            weight: 0.23123
        },
        {
            source: 1,
            target: 4,
            weight: 0.23123
        },
        {
            source: 1,
            target: 5,
            weight: 0.23123
        },
    ]
};
